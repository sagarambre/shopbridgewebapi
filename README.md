# ShopBridge Dot Net Core Web Api

# Prerequisite
* .NET Core 3.1
* MySQL 5.7
* appsettings.json

### How do I get started?
1. Clone this repo
2. Checkout master branch
3. Excute shopbridge.sql in mysql.
4. Update appsettings.json with db connection string
5. Open project in Visual Studio
6. Select ShopBridgeWebApi in Debug profile and press f5. (app will run on 8443 port if ShopBridgeWebApi is selected in debug profile else if iis express is selected it will run on diff port.)