using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShopBridgeWebApi
{
    public class Program
    {
        public static void Main(string[] args)
        {
            string env = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            Console.WriteLine("init main....... ASPNETCORE_ENVIRONMENT {0}", env);
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
            .ConfigureAppConfiguration((hostingContext, config) =>
            {
                IHostEnvironment env = hostingContext.HostingEnvironment;
                config.AddJsonFile("hosting.json", optional: false, reloadOnChange: true);
                config.AddEnvironmentVariables();
            })
            .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.ConfigureKestrel((hostingContext, serverOptions) =>
                    {
                        IHostEnvironment env = hostingContext.HostingEnvironment;

                        string apiport = hostingContext.Configuration.GetSection(env.EnvironmentName).GetValue<String>("port");
                        serverOptions.ListenAnyIP(Convert.ToInt32(apiport));

                    });
                    webBuilder.UseStartup<Startup>();
                });
    }
}
