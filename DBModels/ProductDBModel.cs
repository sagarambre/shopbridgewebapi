﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShopBridgeWebApi.DBModels
{
    public class ProductDBModel
    {
        public int ID { get; set; } 
        public string Code { get; set; } 
        public string Name { get; set; }
         
        public string Description { get; set; }
        public float Price { get; set; }

        public DateTime CreatedAt { get; set; }

        public DateTime UpdatedAt { get; set; }

        public bool IsDeleted { get; set; }
    }
}
