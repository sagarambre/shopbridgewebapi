﻿using ShopBridgeWebApi.APIModels;
using ShopBridgeWebApi.DBModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShopBridgeWebApi.DBMangers
{
    public interface IProductManger
    {
        public Task<List<ProductDBModel>> GetAll();
        public Task<ProductDBModel>Add(ProductModel oModel);
        public Task<ProductDBModel> Update(int id,ProductModel oModel);
        public Task<ProductDBModel> Delete(int id);
    }
}
