﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Threading.Tasks;

namespace ShopBridgeWebApi.DBMangers
{
    public static class DbCommandExt
    {
        public static DbCommand CreateDbCMD(this DbConnection con, CommandType cmdtype, string cmdtext)
        {
            DbCommand cmd = con.CreateCommand();
            cmd.CommandType = cmdtype;
            cmd.CommandText = cmdtext;
            cmd.Connection = con;
            return cmd;
        }

        public static void AddCMDParam(this DbCommand cmd, string parametername, object value)
        {
            DbParameter param = cmd.CreateParameter();
            param.ParameterName = parametername;
            param.Value = value;
            cmd.Parameters.Add(param);
        }
       

        public static bool IsDBNull(this DbDataReader dataReader, string columnName)
        {
            return dataReader[columnName] == DBNull.Value;
        }
    }
}
