﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Threading.Tasks;

namespace ShopBridgeWebApi.DBMangers
{
    public static class DbConnectionFactory
    {
        public static DbConnection GetDbConnection(IServiceProvider serviceProvider)
        {
            IConfiguration Configuration = serviceProvider.GetRequiredService<IConfiguration>();
            string dbconstr = Configuration["DBConstr"];
            return new MySqlConnection(dbconstr);
        }
    }
}
