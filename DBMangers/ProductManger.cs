﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data;
using ShopBridgeWebApi.APIModels;
using ShopBridgeWebApi.DBModels;
using System.Data.Common;

namespace ShopBridgeWebApi.DBMangers
{
    public class ProductManger : IProductManger
    {
        private readonly DbConnection oDbConnection;

        public ProductManger(DbConnection dbcon)
        {
            oDbConnection = dbcon;
        }

        public async Task<List<ProductDBModel>>GetAll()
        {
            List<ProductDBModel> oProductlist = new List<ProductDBModel>();
            DbCommand oDbCommand = oDbConnection.CreateDbCMD(CommandType.StoredProcedure, "sp_get_all_product");
            await oDbConnection.OpenAsync();
            DbDataReader reader = await oDbCommand.ExecuteReaderAsync();
            if (reader.HasRows)
            {
                while (await reader.ReadAsync())
                {
                    ProductDBModel data = new ProductDBModel
                    {
                        ID = Convert.ToInt32(reader["product_id"]),
                        Name = reader["name"].ToString(),
                        Description = reader["description"].ToString(),
                        Price = float.Parse(reader["price"].ToString()),
                        Code = reader["code"].ToString(),
                        CreatedAt = Convert.ToDateTime(reader["created_at"]),
                        UpdatedAt = Convert.ToDateTime(reader["updated_at"]),
                    };
                    oProductlist.Add(data);
                }
            }
            oDbConnection.Close();
            oDbConnection.Dispose();
            return oProductlist;
        }

        public async Task<ProductDBModel> Add(ProductModel oModel)
        {
            ProductDBModel result = null;
            DbCommand oDbCommand = oDbConnection.CreateDbCMD(CommandType.StoredProcedure, "sp_add_product");
            oDbCommand.AddCMDParam("name_in", oModel.Name);
            oDbCommand.AddCMDParam("description_in", oModel.Description);
            oDbCommand.AddCMDParam("price_in", oModel.Price);
            oDbCommand.AddCMDParam("code_in", oModel.Code);
            await oDbConnection.OpenAsync();
            DbDataReader reader = await oDbCommand.ExecuteReaderAsync();
            if(reader.HasRows)
            {
                while (await reader.ReadAsync())
                {
                    result = new ProductDBModel {
                        ID = Convert.ToInt32(reader["product_id"]),
                        Name = reader["name"].ToString(),
                        Description = reader["description"].ToString(),
                        Price= float.Parse(reader["price"].ToString()),
                        Code = reader["code"].ToString(),
                        CreatedAt = Convert.ToDateTime(reader["created_at"]),
                        UpdatedAt = Convert.ToDateTime(reader["updated_at"]),
                    };
                }
            }

            oDbConnection.Close();
            oDbConnection.Dispose();
            return result;
        }

        public async Task<ProductDBModel> Update(int id, ProductModel oModel)
        {
            ProductDBModel result = null;
            DbCommand oDbCommand = oDbConnection.CreateDbCMD(CommandType.StoredProcedure, "sp_update_product");
            oDbCommand.AddCMDParam("id_in", id);
            oDbCommand.AddCMDParam("name_in", oModel.Name);
            oDbCommand.AddCMDParam("description_in", oModel.Description);
            oDbCommand.AddCMDParam("price_in", oModel.Price);
            oDbCommand.AddCMDParam("code_in", oModel.Code);
            await oDbConnection.OpenAsync();
            DbDataReader reader = await oDbCommand.ExecuteReaderAsync();
            if (reader.HasRows)
            {
                while (await reader.ReadAsync())
                {
                    result = new ProductDBModel
                    {
                        ID = Convert.ToInt32(reader["product_id"]),
                        Name = reader["name"].ToString(),
                        Description = reader["description"].ToString(),
                        Price = float.Parse(reader["price"].ToString()),
                        Code = reader["code"].ToString(),
                        CreatedAt = Convert.ToDateTime(reader["created_at"]),
                        UpdatedAt = Convert.ToDateTime(reader["updated_at"]),
                    };
                }
            }

            oDbConnection.Close();
            oDbConnection.Dispose();
            return result;
        }

        public async Task<ProductDBModel> Delete(int id)
        {
            ProductDBModel result = null;
            DbCommand oDbCommand = oDbConnection.CreateDbCMD(CommandType.StoredProcedure, "sp_delete_product_by_id");
            oDbCommand.AddCMDParam("id_in", id);
            await oDbConnection.OpenAsync();
            DbDataReader reader = await oDbCommand.ExecuteReaderAsync();
            if (reader.HasRows)
            {
                while (await reader.ReadAsync())
                {
                    result = new ProductDBModel
                    {
                        ID = Convert.ToInt32(reader["product_id"]),
                        Name = reader["name"].ToString(),
                        Description = reader["description"].ToString(),
                        Price = float.Parse(reader["price"].ToString()),
                        Code = reader["code"].ToString(),
                        CreatedAt = Convert.ToDateTime(reader["created_at"]),
                        UpdatedAt = Convert.ToDateTime(reader["updated_at"]),
                        IsDeleted = Convert.ToBoolean(reader["is_deleted"])
                    };
                }
            }

            oDbConnection.Close();
            oDbConnection.Dispose();
            return result;
        }

    }
}
