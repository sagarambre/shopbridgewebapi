﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace ShopBridgeWebApi.APIModels
{
    public class ProductModel
    {
        [Required]
        [StringLength(15, ErrorMessage = "The {0} can't be longer than {1} characters")]
        public string Code { get; set; }
        [Required]
        [StringLength(150, ErrorMessage = "The {0} can't be longer than {1} characters")]
        public string Name { get; set; }

        [Required]
        [StringLength(500, ErrorMessage = "The {0} can't be longer than {1} characters")]
        public string Description { get; set; }
        [Required]
        [Range(1, float.MaxValue, ErrorMessage = "The {0} must be greater than {1}.")]
        public float Price { get; set; }


    }
}
