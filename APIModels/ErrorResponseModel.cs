﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShopBridgeWebApi.APIModels
{
    public class ErrorResponseModel
    {
        public string ErrorMessage { get;  }
        public Exception ExceptionDetails { get; }
        public string ExceptionMessage { get; }
        public IDictionary<string, object> PendingDetails { get; }
        public ErrorResponseModel(ModelStateDictionary modelState)
        {
            ErrorMessage = "Invalid Request.";
            PendingDetails = new Dictionary<string, object>();
            PendingDetails = modelState.Keys
                    .SelectMany(key => modelState[key].Errors.Select(x => new KeyValuePair<string, object>(key, x.ErrorMessage)))
                    .ToDictionary(d=> d.Key, d=> d.Value);
        }

        public ErrorResponseModel(string errormsg, Exception exdetails)
        {
            ErrorMessage = errormsg;
            ExceptionMessage = exdetails.InnerException != null ? exdetails.InnerException.Message
                            : exdetails.Message;
            ExceptionDetails = exdetails;
        }

        public ErrorResponseModel(string errormsg)
        {
            ErrorMessage = errormsg;
        }
    }
}
