﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc;
using ShopBridgeWebApi.APIModels;

namespace ShopBridgeWebApi.Controllers
{
    public class ExceptionFilter : ExceptionFilterAttribute
    {
        public override void OnException(ExceptionContext ExContext)
        {
            ObjectResult oResult = new ObjectResult(null);

            oResult.StatusCode = 500;
            oResult.Value = new ErrorResponseModel("Something went wrong contact your system admin.", ExContext.Exception);

            ExContext.Result = oResult;
        }
    }
}
