﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ShopBridgeWebApi.APIModels;
using ShopBridgeWebApi.DBModels;
using ShopBridgeWebApi.DBMangers;

namespace ShopBridgeWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [ExceptionFilter]
    public class ProductController : ControllerBase
    {
        private IProductManger oProductManger;
        public ProductController(IProductManger productManger)
        {
            oProductManger = productManger;
        }
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            List<ProductDBModel> oProductlist = await oProductManger.GetAll();
            return Ok(oProductlist);
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] ProductModel oModel)
        {
            if (ModelState.IsValid)
            {
                ProductDBModel oResponse = await oProductManger.Add(oModel);

                return Ok(oResponse);
            }
            else
            {
                return BadRequest(new ErrorResponseModel(ModelState));
            }
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Update(int id,[FromBody] ProductModel oModel)
        {
            if (ModelState.IsValid)
            {
                ProductDBModel oResponse = await oProductManger.Update(id,oModel);
                if (oResponse == null)
                    return NotFound(new ErrorResponseModel("Requested product is not available."));
                return Ok(oResponse);
            }
            else
            {
                return BadRequest(new ErrorResponseModel(ModelState));
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            if (ModelState.IsValid)
            {
                ProductDBModel oResponse = await oProductManger.Delete(id);
                if (oResponse == null)
                    return NotFound(new ErrorResponseModel("Requested product is not available."));
                return Ok(oResponse);
            }
            else
            {
                return BadRequest(new ErrorResponseModel(ModelState));
            }
        }


    }
}
